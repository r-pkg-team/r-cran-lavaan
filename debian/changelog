r-cran-lavaan (0.6.19-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.

 -- Charles Plessy <plessy@debian.org>  Mon, 30 Sep 2024 09:50:24 +0900

r-cran-lavaan (0.6.18-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Thu, 20 Jun 2024 08:59:47 +0900

r-cran-lavaan (0.6.17-2) unstable; urgency=medium

  * Packaging update
  * Standards-Version: 4.7.0 (routine-update)

 -- Joost van Baal-Ilić <joostvb@debian.org>  Tue, 23 Apr 2024 11:52:26 +0000

r-cran-lavaan (0.6.17-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 08 Jan 2024 20:08:20 +0100

r-cran-lavaan (0.6.16-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 25 Jul 2023 06:13:44 +0200

r-cran-lavaan (0.6.15-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 23 Jun 2023 15:17:03 +0200

r-cran-lavaan (0.6.14-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 20 Feb 2023 13:58:36 +0100

r-cran-lavaan (0.6.13-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 16 Jan 2023 13:50:53 +0100

r-cran-lavaan (0.6.12-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 13 Jul 2022 07:17:07 +0200

r-cran-lavaan (0.6.11-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 04 May 2022 09:43:09 +0200

r-cran-lavaan (0.6.10-1) unstable; urgency=medium

  * Team upload
  * New upstream version
  * Disable reprotest

 -- Andreas Tille <tille@debian.org>  Sat, 12 Feb 2022 19:08:31 +0100

r-cran-lavaan (0.6.9-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 21 Sep 2021 06:41:36 +0200

r-cran-lavaan (0.6.7-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 13 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 02 Sep 2020 14:16:45 +0200

r-cran-lavaan (0.6.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Sat, 16 May 2020 20:59:13 +0200

r-cran-lavaan (0.6.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 12
  * dh-update-R to update Build-Depends
  * Set upstream metadata fields: Archive.

 -- Andreas Tille <tille@debian.org>  Fri, 30 Aug 2019 08:14:04 +0200

r-cran-lavaan (0.6.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper 12
  * Standards-Version: 4.4.0
  * Upstream dropped test suite thus deleting autopkgtest

 -- Andreas Tille <tille@debian.org>  Tue, 16 Jul 2019 08:40:08 +0200

r-cran-lavaan (0.6.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Fri, 28 Sep 2018 19:47:38 +0200

r-cran-lavaan (0.6.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sun, 22 Jul 2018 08:32:44 +0200

r-cran-lavaan (0.6.1-3) unstable; urgency=medium

  * Team upload.
  * Remove libgdal-dev from Build-Depends
  * Standards-Version: 4.1.5

 -- Andreas Tille <tille@debian.org>  Thu, 12 Jul 2018 17:09:38 +0200

r-cran-lavaan (0.6.1-2) unstable; urgency=medium

  * Team upload.
  * Rebuild for R 3.5 transition

 -- Andreas Tille <tille@debian.org>  Fri, 01 Jun 2018 10:57:59 +0200

r-cran-lavaan (0.6.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper 11
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends
  * Secure URI in watch file
  * Add r-cran-codetools to Build-Depends since the build warns if it is
    missing and more serious checks might be applied if codetools is
    present

 -- Andreas Tille <tille@debian.org>  Sat, 26 May 2018 15:54:55 +0200

r-cran-lavaan (0.5.23.1097-1) unstable; urgency=medium

  * Team upload
  * New upstream version
  * Standards-Version: 4.1.1
  * Add README.source to document binary data files

 -- Andreas Tille <tille@debian.org>  Sat, 30 Sep 2017 08:00:39 +0200

r-cran-lavaan (0.5.22-1) unstable; urgency=medium

  * Team Upload

  [ Joost van Baal-Ilić ]
  * debian/copyright: even more explicitly mention that this software is not
    only licensed under GPL-2 but GPL-2+.  Thanks Thorsten Alteholz
    <ftpmaster>.

  [ Andreas Tille ]
  * New upstream version
  * cme fix dpkg-control
  * debhelper 10
  * Convert to dh-r
  * Canonical CRAN homepage
  * d/watch:
     - version=4
     - leave remark suggesting to stop changing upstream version string

 -- Andreas Tille <tille@debian.org>  Tue, 29 Nov 2016 18:40:14 +0100

r-cran-lavaan (0.5.20-2) unstable; urgency=medium

  * Initial Debian release. Closes: #831889
  * debian/copyright: completed.
  * debian/control: update Standards-Version from 3.9.6 to 3.9.8.
  * debian/control: explicitly add r-cran-mass, r-cran-mnormt,
    r-cran-pbivnorm, r-cran-quadprog to Depends.  Thanks Daniel Oberski for
    reporting this.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Wed, 20 Jul 2016 16:25:07 +0200

r-cran-lavaan (0.5.20-1) unstable; urgency=low

  * Private release, for Tilburg University.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Tue, 19 Jan 2016 09:49:33 +0100
