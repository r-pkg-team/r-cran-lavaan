Explanation for binary files inside source package according to
  http://lists.debian.org/debian-devel/2013/09/msg00332.html

Files: data/Demo.growth.rda
Documented: man/Demo.growth.Rd
  A toy dataset containing measures on 4 time points (t1,t2, t3 and t4),
  two predictors (x1 and x2) influencing the random intercept and slope, and
  a time-varying covariate (c1, c2, c3 and c4).

Files: data/FacialBurns.rda
Documented: man/FacialBurns.Rd
  A dataset from the Dutch burn center (http://www.adbc.nl).
  The data were used to examine psychosocial functioning in patients with
  facial burn wounds. Psychosocial functioning was measured by
  Anxiety and depression symptoms (HADS), and self-esteem
  (Rosenberg's self-esteem scale).

Files: data/HolzingerSwineford1939.rda
Documented: man/HolzingerSwineford1939.Rd
  The classic Holzinger and Swineford (1939) dataset consists of mental
  ability test scores of seventh- and eighth-grade children from two
  different schools (Pasteur and Grant-White). In the original dataset
  (available in the \code{MBESS} package), there are scores for 26 tests.
  However, a smaller subset with 9 variables is more widely used in the
  literature (for example in Joreskog's 1969 paper, which also uses the 145
  subjects from the Grant-White school only).

Files: data/PoliticalDemocracy.rda
Documented: man/PoliticalDemocracy.Rd
  The `famous' Industrialization and Political Democracy dataset. This dataset is
  used throughout Bollen's 1989 book (see pages 12, 17, 36 in chapter 2, pages
  228 and following in chapter 7, pages 321 and following in chapter 8).
  The dataset contains various measures of political democracy and
  industrialization in developing countries.

 -- Andreas Tille <tille@debian.org>  Sat, 30 Sep 2017 07:50:08 +0200
